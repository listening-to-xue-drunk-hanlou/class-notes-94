/**
 * 目标二：压缩 js 里代码，并整合到 html 中一起运行
 *  2.1 读取 public/index.js 内容
 *  2.2 使用正则替换内容字符串里的，回车符\r 换行符\n 打印语句console.log('xxx');
 *  2.3 确认后，拼接 html 内容写入到 dist/index.html 内
 */
//引入模块
const fs = require('fs')
const path = require('path')

//1.1 读取 public/index.html内容
fs.readFile(path.join(__dirname, 'public/index.html'), (err, data) => {
  if (err) return console.log(err)
  let htmlResult = data.toString().replace(/\r|\n/g, '').replace(/\s+/g, ' ')

  fs.readFile(path.join(__dirname, 'public/index.js'), (err, data) => {
    if (err) return console.log(err)
    const jsResult = data.toString().replace(/\r|\n/g, '').replace(/\s+/g, ' ')
    htmlResult += `<script>${jsResult}</script>`

    fs.writeFile(path.join(__dirname, 'dist/index.html'), htmlResult, err => {
      if (err) return console.log(err)
      console.log('压缩完成')
    })
  })
})
