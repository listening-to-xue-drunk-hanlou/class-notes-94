/**
 * 目标：使用 fs 模块，读写文件内容
 * 语法：
 * 1. 引入 fs 模块
 * 2. 调用 writeFile 写入内容
 * 3. 调用 readFile  读取内容
 */

//1.引入 fs 模块
const fs = require('fs')

//2.写入内容
// 参数1:文件路径(如果文件不存在会自动创建文件)
// 参数2：写入的内容
// 参数3：回调函数,写入完成时触发,会传入一个 err 对象
// fs.writeFile('./123.txt', '今天早上起床吃早餐,看到道长很牛逼', err => {
//     console.log('写入完成, 错误是:', err)
// })


//3.读取文件
//参数1: 文件路径
//参数2: 回调函数,读取完成时触发, 会传入一个 err对象和 一个data 数据
//data 的数据类型时 Buffer ,是一种在计算机中表示数据流的格式,可以理解为 2 进制的压缩版
//为什么要这样设计?因为我们读取的文件有可能是文本, 也有可能是图片 / 音频 /视频, 如果它给你的是字符串, 则太受限制了
//可以理解为Buffer就是一种数据的底层格式
//给到我们之后只需要自行转换即可,例如: toString()
fs.readFile('./123.text', (err, data) => {
    console.log('错误是:', err)
    console.log('数据是:', data)
})