/**
 * 目标：基于 Web 服务，开发-城市列表数据接口
 * 步骤：
 *  1. 判断 req.url 资源路径+查询字符串，路径前缀匹配/api/city
 *  2. 借助 querystring 模块的方法，格式化查询参数字符串
 *  3. 读取 city.json 城市数据，匹配省份名字下属城市列表
 *  4. 返回城市列表，启动 Web 服务测试
 */

const http = require('http')
const fs = require('fs')
const path = require('path')
const server = http.createServer()
const qs = require('querystring')

server.on('request', (req, res) => {
  // req.socket.remoteAddress 可以获取客户端的 ip 地址
  console.log('有朋友来了:', req.socket.remoteAddress)
  if (req.url === '/api/province') {
    fs.readFile(path.join(__dirname, './data/province.json'), (err, data) => {
      res.setHeader('Content-Type', 'application/json')
      res.end(data.toString())
    })
  } else if (req.url.startsWith('/api/city')) {
    // /api/city?pname=湖北省 => 返回湖北省下面所有的城市
    // console.log('返回城市数据:', req.url.split('?')[1])
    const queryStr = req.url.split('?')[1]
    // qs 模块的作用: 解析查询字符串
    // qs.parse(): 将查询字符串转成对象, 例如: pname=湖北省&cname=武汉市 => { pname: '湖北省', cname: '武汉市' }
    // qs.stringify(): 将对象转成查询字符串, 例如: { pname: '湖北省', cname: '武汉市' } => pname=湖北省&cname=武汉市
    const obj = qs.parse(queryStr)
    console.log(obj.pname)

    // 根据省份名称找到对应的城市数组并响应给客户端
    // 1. 读取 city.json 获取所有的城市
    fs.readFile(path.join(__dirname, './data/city.json'), (err, data) => {
      // 2. 将 JSON 字符串转成对象
      const cityObj = JSON.parse(data.toString())
      // 3. 去城市数据中找到对应的城市
      // console.log(cityObj[obj.pname])
      const result = cityObj[obj.pname]
      res.setHeader('Content-Type', 'application/json')
      // http 协议只支持传输两种类型的数据: 1. 字符串 2. 二进制字节流(Buffer)
      // 所以需要将数组转换成 JSON 字符串再响应给客户端
      // 4. 响应给客户端
      res.end(JSON.stringify(result))
    })
  } else {
    res.setHeader('Content-Type', 'text/html;charset=utf-8')
    res.end('您要找的资源不存在!小傻瓜!')
  }
})

server.listen(3000, () => {
  console.log('服务已启动')
})
