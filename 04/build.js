/**
 * 目标一：压缩 html 里代码
 * 需求：把 public/index.html 里的，回车/换行符去掉，写入到 dist/index.html 中
 *  1.1 读取 public/index.html 内容
 *  1.2 使用正则替换内容字符串里的，回车符\r 换行符\n
 *  1.3 确认后，写入到 dist/index.html 内
 */

//引入模块
const fs = require('fs')
const path = require('path')

//1.1 读取 public/index.html内容
fs.readFile(path.join(__dirname, 'public/index.html'), (err, data) => {
    if(err) return console.log(err)
    //g 开启全局匹配
    // \r 回车 \n 换行 | 或
    // console.log(data.toString().replace(/\r|\n/g, '').replace(/\s+/g, ' '))
    const result = data.toString().replace(/\r|\n/g, '').replace(/\s+/g, ' ')
    fs.writeFile(path.join(__dirname, 'dist/index.html'), result, err => {
        if(!err) console.log('压缩完成')
    })
})





