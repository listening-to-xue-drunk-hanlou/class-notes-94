/**
 * 目标：使用 http 模块，创建 Web 服务
 * Web服务：一个程序，用于提供网上信息浏览服务
 * 步骤：
 *  1. 引入 http 模块，创建 Web 服务对象
 *  2. 监听 request 事件，对本次请求，做一些响应处理
 *  3. 启动 Web 服务监听对应端口号
 *  4. 运行本服务在终端，用浏览器访问 http://localhost:3000/ 发起请求（localhost 是本机域名）
 * 注意：终端里启动了服务，如果想要终止按 ctrl c 停止即可
 */

// 1. 引入 http 模块，创建 Web 服务对象
const http = require('http')
const server = http.createServer()


// 2. 监听 request 事件，对本次请求，做一些响应处理
//事件处理函数有2个参数
//req: 请求对象
//res: 响应对象
server.on('request', (req, res) => {
    //有人请求我就给他响应: HelloWorld
    res.end('HelloWorld')
})

//3.启动 Web 服务监听对应端口号
server.listen(3000, () => {
    console.log('服务器已启动')
})

//4.打开浏览器访问本机的 3000 端口即可:http://localhost:3000

//注意事项:
//1.服务器程序一旦启动就会进入阻塞状态, 等待用户请求,不要关闭终端, 否则服务器程序终止
//2.不要连续两次启动服务器程序, 因为端口号只能被一个服务占用
//3.修改服务器程序的代码后, 记得重新启动一下服务器程序