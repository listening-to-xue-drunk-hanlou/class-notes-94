/**
 * 目标：读取 test.txt 文件内容
 * 注意：代码中，使用绝对路径
 * 原因：Node.js 执行时会以终端所在文件夹作为相对路径，去拼接代码中路径使用（导致找不到目标文件）
 * 解决：使用 path.join() 和 __dirname 来填写要查找的目标文件绝对地址
 */


//2.读取文件:这里的相对路径相对的是运行程序时所在的位置
//_dirname: 全局变量, 可以获取当前文件所在目录的绝对路径
//console.log(_dirname)

//path 模块: 专门处理路径字符串的模块
//path.join() 拼接路径字符串
//传入 n 个参数, 来实现路径字符串的拼接
// console.log(path.join(__dirname, '..', '..', 'abc.txt'))
//在 node中只建议使用绝对路径,而需要用绝对路径就必须要_dirname + path 模块来配合
//fs.reaFile('../123.txt', (err, data) => {})
// console.log(path.join(__dirname, '../123.txt'))
fs.readFile(path.join(__dirname, '../123.txt'), (err, data) => {
    if(err) return console.log(err)
    console.log(data.toString())
})